package pl.szykoc.movrev.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.szykoc.movrev.data.Review;
import pl.szykoc.movrev.data.ReviewRepository;

import java.util.Date;

@Configuration
@Slf4j
public class LoadDB {

    @Bean
    CommandLineRunner initDatabase(ReviewRepository repository) {
        return args -> {
            log.info("Inserting to database " + repository.save(new Review("TestDescription", new Date())));
            log.info("Inserting to database " + repository.save(new Review("TestDescription2", new Date())));
        };
    }


}
