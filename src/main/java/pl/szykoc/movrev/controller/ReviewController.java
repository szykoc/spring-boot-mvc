package pl.szykoc.movrev.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.szykoc.movrev.data.ReviewRepository;
import pl.szykoc.movrev.services.ReviewService;

@Controller
public class ReviewController {

    private ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/reviews/{id}")
    public String review(Model model, @PathVariable Long id) {
        model.addAttribute("review", reviewService.findById(id));
        return "review";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/reviewsAll")
    public String review(Model model, @RequestParam(value = "count", defaultValue = "0") int count) {
        if (count == 0){
            model.addAttribute("reviewList", reviewService.findAllReviews());
        } else {
            model.addAttribute("reviewList", reviewService.findReviewsByCount(count));
        }
        return "reviews";
    }
}
