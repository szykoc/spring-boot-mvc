package pl.szykoc.movrev.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.szykoc.movrev.data.User;
import pl.szykoc.movrev.data.UserRepository;
import pl.szykoc.movrev.services.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegistrationForm(@ModelAttribute User user) {
        return "registerForm";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String processRegistrationForm(@ModelAttribute(value = "user") @Valid User user, Errors errors) {
        if (errors.hasErrors()){
            return "registerForm";
        }
        userService.saveUser(user);
        return "redirect:/user/" + user.getUserName();
    }

    @RequestMapping(value = "/{userName}", method = RequestMethod.GET)
    public String showUserProfile(@PathVariable String userName, Model model) {
        User user = userService.findByFirstName(userName);
        model.addAttribute("user", user);
        return "userProfile";
    }


}
