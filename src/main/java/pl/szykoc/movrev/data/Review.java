package pl.szykoc.movrev.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Entity
@Data
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String description;
    private Date postedDate;

    public Review() {
    }

    public Review(String description, Date postedDate) {
        this.description = description;
        this.postedDate = postedDate;
    }
}
