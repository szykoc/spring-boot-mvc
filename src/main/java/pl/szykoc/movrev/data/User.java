package pl.szykoc.movrev.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String firstName;
    @Size(min = 1, max = 5)
    private String lastName;
    private String nick;
    private String userName;
    private String password;

    public User() {
    }

    public User(String firstName, @NotNull String lastName, String nick, String userName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nick = nick;
        this.userName = userName;
        this.password = password;
    }

}
