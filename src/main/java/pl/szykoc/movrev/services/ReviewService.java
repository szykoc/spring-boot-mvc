package pl.szykoc.movrev.services;

import org.springframework.stereotype.Component;
import pl.szykoc.movrev.data.Review;
import pl.szykoc.movrev.data.ReviewRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ReviewService {

    private final ReviewRepository reviewRepository;

    public ReviewService(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public List<Review> findAllReviews(){
        return (List)reviewRepository.findAll();
    }

    public Review findById(Long id){
        return reviewRepository.findById(id).orElse(null);
    }

    public List<Review> findReviewsByCount(int count) {
        List<Review> allReviews = (List<Review>) reviewRepository.findAll();

/*        StreamSupport.stream(reviewRepository.findAll().spliterator(), false)
                .limit(count)
                .collect(Collectors.toList());*/

        return allReviews.stream()
                .limit(count)
                .collect(Collectors.toList());

    }
}
