package pl.szykoc.movrev.services;

import org.springframework.stereotype.Component;
import pl.szykoc.movrev.data.User;
import pl.szykoc.movrev.data.UserRepository;

@Component
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User findByFirstName(String userName) {
        return userRepository.findByUserName(userName);
    }
}
