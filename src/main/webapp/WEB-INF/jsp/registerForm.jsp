<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%--
  Created by IntelliJ IDEA.
  User: szymo
  Date: 08.05.2019
  Time: 22:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome, register here</title>
    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/resources/style.css" />" >

</head>
<body>
    <h1>Registration form</h1>
    <sf:form method="POST" modelAttribute="user">
        First Name: <sf:input path="firstName" /> <sf:errors path="firstName" cssClass="error" /><br/>
        Last name: <sf:input path="lastName" /> <sf:errors path="lastName" cssClass="error" /><br/>
        Username: <sf:input path="userName" /> <sf:errors path="userName" cssClass="error" /><br/>
        Password:   <sf:input path="password" /> <sf:errors path="password" cssClass="error" /><br/>
        <input type="submit" name="Register"/> <br/>
    </sf:form>
</body>
</html>
