<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: szymo
  Date: 07.05.2019
  Time: 22:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Reviews</title>
    <link href="webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <h2>Recent Reviews</h2>
    <c:forEach items="${reviewList}" var="reviews">
        <li id="review_<c:out value="reviews.id"/>">
            <div class="reviewMessage">
                <c:out value="${reviews.description}"/>
            </div>
            <span class="reviewTime"><c:out value="${reviews.postedDate}"/></span>
        </li>
    </c:forEach>


</body>
</html>
