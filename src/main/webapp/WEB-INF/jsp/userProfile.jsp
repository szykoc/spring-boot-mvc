<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: szymo
  Date: 09.05.2019
  Time: 12:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User profile</title>
</head>
<body>
    <h1>Your profile</h1>
    <c:out value="${user.nick}"/><br/>
    <c:out value="${user.firstName}"/> <c:out value="${user.lastName}"/>
</body>
</html>
