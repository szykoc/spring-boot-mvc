package pl.szykoc.movrev;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.szykoc.movrev.controller.ReviewController;
import pl.szykoc.movrev.data.Review;
import pl.szykoc.movrev.services.ReviewService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

public class ReviewControllerTest {

    @Test
    public void shouldShowAllReviews() throws Exception {
        //given
        ReviewService reviewServiceMock = mock(ReviewService.class);
        ReviewController reviewController = new ReviewController(reviewServiceMock);

        //when
        Mockito.when(reviewServiceMock.findAllReviews()).thenReturn(createExpectedReviews());
        MockMvc reviewControllerMock = MockMvcBuilders.standaloneSetup(reviewController).build();

        //then
        reviewControllerMock.perform(get(
                "/reviewsAll"))
                .andExpect(model().attributeExists("reviewList"))
                .andExpect(model().attribute("reviewList", hasItems(createExpectedReviews().toArray())));

    }

    private List<Review> createExpectedReviews() {
        return Arrays.asList(
                new Review("Test", new Date()),
                new Review("Test2", new Date())

        );
    }
}