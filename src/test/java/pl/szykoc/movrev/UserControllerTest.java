package pl.szykoc.movrev;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.szykoc.movrev.controller.UserController;
import pl.szykoc.movrev.data.User;
import pl.szykoc.movrev.data.UserRepository;
import pl.szykoc.movrev.services.UserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UserControllerTest {

    @Test
    public void testShowRegistrationForm() throws Exception {
        UserService userService = Mockito.mock(UserService.class);
        UserController reviewerController = new UserController(userService);
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(reviewerController).build();

        mockMvc.perform(get("/user/register"))
                .andExpect(view().name("registerForm"));
    }

    @Test
    public void shouldProcessRegistrationForm() throws Exception {
        UserService userService = Mockito.mock(UserService.class);

        User userToSave = new User("Thomas", "Jones", "thom12", "thom", "password");

        Mockito.when(userService.saveUser(userToSave)).thenReturn(userToSave);

        UserController userController = new UserController(userService);
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(userController).build();

        mockMvc.perform(post("/user/register")
                .param("firstName", "Thomas")
                .param("lastName", "Jones")
                .param("nick", "thom12")
                .param("userName", "thom")
                .param("password", "password"))
                .andExpect(redirectedUrl("/user/thom"));

        Mockito.verify(userService, Mockito.atLeastOnce()).saveUser(userToSave);

    }
}